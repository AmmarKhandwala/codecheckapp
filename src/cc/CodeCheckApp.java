package cc;

import java.util.Locale;
import cc.data.CodeCheckData;
import cc.file.CodeCheckFile;
import cc.workspace.extractWorkspace;
import cc.workspace.renameWorkspace;
import cc.workspace.welcomeDilogue;
import djf.AppTemplate;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;

/**
 * This class serves as the application class for our Slideshow Creator App. 
 * Note that much of its behavior is inherited from AppTemplate, as defined in
 * the Desktop Java Framework. This app starts by loading all the UI-specific
 * settings like icon files and tooltips and other things, then the full 
 * User Interface is loaded using those settings. Note that this is a 
 * JavaFX application.
 * 
 * @author Ammar Khandwala
 * @version 1.0
 */
public class CodeCheckApp extends AppTemplate {
    /**
     * This hook method must initialize all four components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    
    @Override
    public void buildAppComponentsHook() {
        
        // CONSTRUCT ALL FOUR COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, SO BE CAREFUL OF THE ORDER
        welcomeDilogue welcome = new welcomeDilogue(this);
        dataComponent = new CodeCheckData(this);
        workspaceComponent = new extractWorkspace(this);
        fileComponent = new CodeCheckFile(this);   
        welcome.primary().showAndWait();
        
  
    }
    
   
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
        
	Locale.setDefault(Locale.US);
	launch(args);
    }
}