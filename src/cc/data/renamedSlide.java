/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Ammar Khandwala
 */
public class renamedSlide {
     private StringProperty fileNameProperty;
    
    
    public renamedSlide(String initFileName) {
        
        fileNameProperty = new SimpleStringProperty(initFileName);
    }
     // ACCESSORS AND MUTATORS
    
    public StringProperty getFileNameProperty() {
        return fileNameProperty;
    }
    public String getFileName() {
        return fileNameProperty.getValue();
    }
    public void setFileName(String initFileName) {
        fileNameProperty.setValue(initFileName);
    }
    
 
}
