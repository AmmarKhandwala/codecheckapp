package cc.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import cc.CodeCheckApp;

/**
 * This is the data component for CodeCheckApp. It has all the data needed to be
 * set by the user via the User Interface and file I/O can set and get all the
 * data from this object
 *
 * @author Ammar Khandwala
 */
public class CodeCheckData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CodeCheckApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Slide> slides;
    ObservableList<renamedSlide> renameSlides;
    ObservableList<unzipSlide> unzipSlides;
    ObservableList<sourceCodeSlide> sourceSlides;
    ObservableList<codecheckSlide> codecheckSlides;

    /**
     * This constructor will setup the required data structures for use.
     *
     * @param initApp The application this data manager belongs to.
     */
    public CodeCheckData(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // MAKE THE SLIDES MODEL
        slides = FXCollections.observableArrayList();
        renameSlides = FXCollections.observableArrayList();
        unzipSlides = FXCollections.observableArrayList();
        sourceSlides = FXCollections.observableArrayList();
        codecheckSlides = FXCollections.observableArrayList();
    }

    // ACCESSOR METHOD
    public ObservableList<Slide> getSlides() {
        return slides;
    }
    
    public ObservableList<unzipSlide> getUnzipSlides() {
        return unzipSlides;
    }
    
    public ObservableList<renamedSlide> getrenamedSlides() {
        return renameSlides;
    }
    
    public ObservableList<sourceCodeSlide> getSourceSlides() {
        return sourceSlides;
    }
    
     public ObservableList<codecheckSlide> getcodeCheckSlides() {
        return codecheckSlides;
    }

    /**
     * Called each time new work is created or loaded, it resets all data and
     * data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
       
    }

    // FOR ADDING A SLIDE WHEN THERE ISN'T A CUSTOM SIZE
    public void addSlide(String fileName) {
        Slide slideToAdd = new Slide(fileName);
        slides.add(slideToAdd);
    }
    
    public void addRenameSlide(String fileName) {
        renamedSlide slideToAdd = new renamedSlide(fileName);
        renameSlides.add(slideToAdd);
    }
    
    public void addUnzipSlide(String fileName) {
        unzipSlide slideToAdd = new unzipSlide(fileName);
        unzipSlides.add(slideToAdd);
    }
    
     public void addSourceSlide(String fileName) {
        sourceCodeSlide slideToAdd = new sourceCodeSlide(fileName);
        sourceSlides.add(slideToAdd);
    }
     
     public void addCodeCheckSlide(String fileName) {
        codecheckSlide slideToAdd = new codecheckSlide(fileName);
        codecheckSlides.add(slideToAdd);
    }
     
    public boolean checkDuplicate(String name){
        Boolean value= false;
        for (int i=0; i<slides.size();i++){
            if (slides.get(i).getFileName().equals(name)) {
                value= true;
            }
        }
        return value;
    }
    
    public boolean checkRenameDuplicate(String name){
         Boolean value= false;
        for (int i=0; i<renameSlides.size();i++){
            if (renameSlides.get(i).getFileName().equals(name)) {
                value= true;
            }
        }
        return value;
    }
    
     public boolean checkUnzipDuplicate(String name){
         Boolean value= false;
        for (int i=0; i<unzipSlides.size();i++){
            if (unzipSlides.get(i).getFileName().equals(name)) {
                value= true;
            }
        }
        return value;
    }
     public boolean checkSourceDuplicate(String name){
         Boolean value= false;
        for (int i=0; i<sourceSlides.size();i++){
            if (sourceSlides.get(i).getFileName().equals(name)) {
                value= true;
            }
        }
        return value;
    }
     public boolean checkCodecheckDuplicate(String name){
         Boolean value= false;
        for (int i=0; i<codecheckSlides.size();i++){
            if (codecheckSlides.get(i).getFileName().equals(name)) {
                value= true;
            }
        }
        return value;
    }
    }

