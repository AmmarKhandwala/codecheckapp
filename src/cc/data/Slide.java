package cc.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This data class represents a single slide in a slide show. Note that
 * it has information regarding the path to the file, the caption for
 * the slide, and sizing information.
 * @author Ammar Khandwala
 */
public class Slide {
    private StringProperty fileNameProperty;
    
    
    public Slide(String initFileName) {
        
        fileNameProperty = new SimpleStringProperty(initFileName);
    }
    
    // ACCESSORS AND MUTATORS
    
    
    public StringProperty getFileNameProperty() {
        return fileNameProperty;
    }
    public String getFileName() {
        return fileNameProperty.getValue();
    }
    
    public void setFileName(String initFileName) {
        fileNameProperty.setValue(initFileName);
    }
    
   
}
