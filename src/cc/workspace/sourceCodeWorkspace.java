/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.EXTRACT_BUTTON_TEXT;
import static cc.CodeCheckProp.EXTRACT_SOURCE_BUTTON_TEXT;
import static cc.CodeCheckProp.LABEL_EXTRACT_SOURCE;
import static cc.CodeCheckProp.LABEL_EXTRACT_SOURCE_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_EXTRACT_SOURCE_FILETYPE_LABEl;
import static cc.CodeCheckProp.LABEL_EXTRACT_SOURCE_INFO;
import static cc.CodeCheckProp.LABEL_EXTRACT_SOURCE_PROGRESS;
import static cc.CodeCheckProp.LABEL_UNZIP;
import static cc.CodeCheckProp.LABEL_UNZIP_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_UNZIP_INFO;
import static cc.CodeCheckProp.LABEL_UNZIP_PROGRESS;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.UNZIP_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import cc.data.CodeCheckData;
import cc.data.Slide;
import cc.data.sourceCodeSlide;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_EDIT_TEXT_FIELD;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import properties_manager.PropertiesManager;

/**
 *
 * @author Ammar Khandwala
 */
public class sourceCodeWorkspace {

    CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    HBox bottomToolbar;
    VBox boxLeft, boxRight;
    Button homeButton;
    Button nextButton;
    Button previousButton;
    Button removeButton;
    Button refreshButton;
    Button viewButton;
    Button extractCodeButton;
    CheckBox checkBox1, checkBox2, checkBox3, checkBox4, checkBox5;
    FlowPane flow, flow1, flow2;
    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<sourceCodeSlide> slidesTableView;
    TableColumn<sourceCodeSlide, StringProperty> extractColumn;
    private StringBuilder fieldContent, fieldContent1;

    // THE EDIT PANE
    GridPane editPane;
    Label label;
    Label infoLabel;
    Label progressLabel;
    Label typeLabel;
    ProgressBar progress;
    TextFlow textArea;
    ScrollPane textAreaScrollPane;
    TextField text1;
    BorderPane renameWorkSpaceBorderPane;
    Text extractSuccess;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public sourceCodeWorkspace(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
        bottomToolbar = new HBox();
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        extractCodeButton = new Button(props.getProperty(EXTRACT_SOURCE_BUTTON_TEXT));

        slidesTableScrollPane = new ScrollPane();
        textAreaScrollPane = new ScrollPane();
        flow = new FlowPane();
        flow1 = new FlowPane();
        flow2 = new FlowPane();
        boxLeft = new VBox();
        boxRight = new VBox();
        slidesTableView = new TableView();
        extractColumn = new TableColumn(props.getProperty(LABEL_EXTRACT_SOURCE_COLUMN_TEXT));
        editPane = new GridPane();
        label = new Label(props.getProperty(LABEL_EXTRACT_SOURCE));
        infoLabel = new Label(props.getProperty(LABEL_EXTRACT_SOURCE_INFO));
        progressLabel = new Label(props.getProperty(LABEL_EXTRACT_SOURCE_PROGRESS));
        typeLabel = new Label(props.getProperty(LABEL_EXTRACT_SOURCE_FILETYPE_LABEl));
        progress = new ProgressBar();
        checkBox1 = new CheckBox(".java");
        checkBox2 = new CheckBox(".js");
        checkBox3 = new CheckBox(".c, .h, .cpp");
        checkBox4 = new CheckBox(".cs");
        text1 = new TextField();
        checkBox5 = new CheckBox(text1.getText());
        extractSuccess = new Text("Successfull Code Extraction:\n");
        textArea = new TextFlow();
        textArea.setTextAlignment(TextAlignment.JUSTIFY);
        textAreaScrollPane.setContent(textArea);
        fieldContent1 = new StringBuilder("");
        // ARRANGE THE TABLE
        extractColumn = new TableColumn(props.getProperty(LABEL_EXTRACT_SOURCE_COLUMN_TEXT));

        slidesTableView.getColumns().add(extractColumn);

        extractColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(1));

        extractColumn.setCellValueFactory(
                new PropertyValueFactory<sourceCodeSlide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<sourceCodeSlide> model = data.getSourceSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
        flow.getChildren().add(checkBox1);
        flow.getChildren().add(checkBox2);
        flow1.getChildren().add(checkBox3);
        flow1.getChildren().add(checkBox4);
        flow2.getChildren().add(checkBox5);
        flow2.getChildren().add(text1);
        bottomToolbar.getChildren().add(removeButton);
        bottomToolbar.getChildren().add(refreshButton);
        bottomToolbar.getChildren().add(viewButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(label, 0, 0);
        editPane.add(infoLabel, 0, 1);
        editPane.add(progressLabel, 1, 0);
        editPane.add(progress, 2, 0);
        editPane.add(extractCodeButton, 1, 1);
        editPane.add(slidesTableScrollPane, 0, 2);
        editPane.add(bottomToolbar, 0, 3);
        editPane.add(textAreaScrollPane, 1, 2);
        editPane.add(typeLabel, 0, 4);
        editPane.add(flow, 0, 5);
        editPane.add(flow1, 0, 6);
        editPane.add(flow2, 0, 7);

        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        textAreaScrollPane.setFitToWidth(true);
        textAreaScrollPane.setFitToHeight(true);
        slidesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        slidesTableScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        textAreaScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        //controller = new CodeCheckController(app);
        extractCodeButton.setOnAction((ActionEvent e) -> {
            extractSourceCode();
        });

        removeButton.setOnAction((ActionEvent e) -> {
            remove();
        });
        
         viewButton.setOnAction((ActionEvent e) -> {
            view();
        });
        
        

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {

        bottomToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        flow.getStyleClass().add(CLASS_BORDERED_PANE);
        flow1.getStyleClass().add(CLASS_BORDERED_PANE);
        flow2.getStyleClass().add(CLASS_BORDERED_PANE);
        extractCodeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        boxLeft.getStyleClass().add(CLASS_BORDERED_PANE);
        boxRight.getStyleClass().add(CLASS_BORDERED_PANE);
        label.getStyleClass().add(CLASS_PROMPT_LABEL);
        progressLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        infoLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        typeLabel.getStyleClass().add(CLASS_PROMPT_LABEL);

    }

    public GridPane getExtractSourceWorkspace() {
        return editPane;
    }

    public void extractSourceCode() {
        fieldContent = new StringBuilder("");
        ArrayList<File> file = new ArrayList();
        File parent = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName());
        searchForDatFiles(parent, file);
        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File projects = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "code/NetID/"
                    + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
            projects.mkdir();
            try {
                Writer output = null;
                for (int j = 0; j < file.size(); j++) {

                    output = new BufferedWriter(new FileWriter(projects + "/" + file.get(j).getName()));

                }

                output.close();
                System.out.println("File has been written");

            } catch (Exception e) {
                System.out.println("Could not create file");
            }
        }
         File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID");
        for (File listFile : f.listFiles()) {
            
               fieldContent.append(listFile.getName()).append("\n");
                
            
        }
       
        Text text = new Text();
        Text extractError = new Text("Extraction Error");

        text.setText(fieldContent.toString());
        textArea.getChildren().clear();
        textArea.getChildren().addAll(extractSuccess, text);
    }

    public void searchForDatFiles(File root, List<File> datOnly) {
        //Listeners
        checkBox1.setOnAction((ActionEvent e) -> {
            String s = checkBox1.getText();
            System.out.println(s);
        });
        checkBox2.setOnAction((ActionEvent e) -> {
            String t = checkBox2.getText();
        });
        checkBox3.setOnAction((ActionEvent e) -> {
            String u = checkBox3.getText();
        });

        checkBox4.setOnAction((ActionEvent e) -> {
            String v = checkBox4.getText();
        });

        checkBox5.setOnAction((ActionEvent e) -> {
            String x = checkBox5.getText();
        });

        if (root == null || datOnly == null) {
            return; //just for safety   
        }
        if (root.isDirectory()) {
            for (File file : root.listFiles()) {
                searchForDatFiles(file, datOnly);
            }

        } else if (root.isFile() && ((root.getName().endsWith(".java")) || (root.getName().endsWith("t")) || (root.getName().endsWith("u")) || (root.getName().endsWith("v")) || (root.getName().endsWith("x")))) {
            datOnly.add(root);
        }
    }

    public void remove() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();

        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/"+ "code/NetID/"+
                    slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
            if (f.exists()) {
                f.delete();
               data.getSourceSlides().removeAll(slidesTableView.getSelectionModel().getSelectedItems());
            }
             else
            dialog.show("Remove Error", "File has not yet been extracted");

        }

    }
    
    public void view() {
        fieldContent1= new StringBuilder("");
        String entryName = new String("");
         AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
       // if (slidesTableView.getSelectionModel().selectionModeProperty().getValue().MULTIPLE)
       // System.out.println(slidesTableView.getSelectionModel().getSelectionMode().equals(SelectionMode.MULTIPLE));
        
            String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID/" 
                    + "/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName();
            File file = new File(Path);
            String[] directories = file.list(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return new File(current, name).isDirectory();
                }
            });
            
                fieldContent1.append(Arrays.toString(directories)).append("\n");
                
                
            
            
            ScrollPane scroll = new ScrollPane();
            Text text = new Text();
            text.setText(fieldContent1.toString());
            scroll.setContent(text);
            Scene scene = new Scene (scroll);
            dialog.setScene(scene);
            dialog.setTitle("Contents");
            dialog.show();
            
       
    }
    
    
}
