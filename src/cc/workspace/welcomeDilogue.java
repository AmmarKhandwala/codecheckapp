/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;


import cc.CodeCheckApp;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
/*

@author Ammar Khandwala

 */

public class welcomeDilogue

{
    CodeCheckApp app;
    Stage primaryStage;
    Scene scene;
    VBox leftBox, rightBox;
    HBox topPane;
    Label welcomeTitle;
    Label recent;
    Label createNew;
    BorderPane workspace ;
    Hyperlink recentLoadWork;
    
    public welcomeDilogue(CodeCheckApp app)
    {          
        this.app = app;
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
    
        initStyle(); 
        
        
        initLayout();
        
    }
    
    private void initLayout(){
        
       primaryStage = new Stage();
       leftBox= new VBox();
       rightBox= new VBox();
       topPane= new HBox();
       welcomeTitle= new Label("Welcome To Code Check");
       recent = new Label ("Recent Code Check");
       createNew = new Label("Create New Code Check");
       workspace= new BorderPane();
        
        
       
        
        
        
        recentLoadWork = new Hyperlink("Ammar3");
        leftBox.getChildren().addAll(recent, recentLoadWork);
        leftBox.setPadding(new Insets(50, 25, 25, 50));
        leftBox.setSpacing(10);
      
   
    
     
                
      //Logo for App
        HBox appLogo = new HBox();
        appLogo.setPadding(new Insets(200, 100, 100, 200));
        appLogo.setSpacing(20);
        appLogo.setStyle("-fx-background-color: #aa8cc5;");
        appLogo.setAlignment(Pos.CENTER);
        Label logo = new Label("Code Check");
        logo.setStyle("-fx-margin-left: 200 px");
        logo.setTextFill(Color.web("#FFFFFF"));
        logo.setFont(Font.font("Times New Roman", FontWeight.BOLD, 80));
        logo.styleProperty().set(CLASS_PROMPT_LABEL);
        appLogo.getChildren().add(logo);
        
       
       
 
       
        rightBox.setPadding(new Insets(150, 100, 100, 150));
        rightBox.setSpacing(10);
        rightBox.setSnapToPixel(true);
        Hyperlink createAccount = new Hyperlink("Create New Code Check");
        rightBox.getChildren().addAll(appLogo, createAccount);
        createAccount.setStyle("-fx-font-size: 20.0");
        
    
        
        workspace.setTop(topPane);
        workspace.setLeft(leftBox);
        workspace.setRight(rightBox);
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        
        primaryStage.setX(primaryScreenBounds.getMinX());
        primaryStage.setY(primaryScreenBounds.getMinY());
        primaryStage.setWidth(primaryScreenBounds.getWidth());
        primaryStage.setHeight(primaryScreenBounds.getHeight());
       

        // Create the Scene
         scene = new Scene(workspace);

        // Add the scene to the Stage
          primaryStage.setScene(scene);

        // Set the title of the Stage
        primaryStage.setTitle("Welcome To Code Check");
        
        createAccount.setOnAction(e->{
            
           app.getGUI().getFileController().handleNewRequest();
           primaryStage.close();
        });
           
        primaryStage.setOnCloseRequest(e -> {
            primaryStage.close();
        });
        
        recentLoadWork.setOnAction(e->{
            app.getGUI().getFileController().handleLoadRequest();
        });
   
        
        
        //primaryStage.show();
        
       
        
        

    }

    private void initStyle() {
        
    }
    
    public Stage primary(){
        return primaryStage;
    }


}
