package cc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import cc.CodeCheckApp;
import static cc.CodeCheckProp.EXTRACT_BUTTON_TEXT;
import static cc.CodeCheckProp.HOME_BUTTON_TEXT;
import static cc.CodeCheckProp.LABEL_EXTRACT;
import static cc.CodeCheckProp.LABEL_EXTRACT_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_EXTRACT_INFO;
import static cc.CodeCheckProp.LABEL_EXTRACT_PROGRESS;
import static cc.CodeCheckProp.NEXT_BUTTON_TEXT;
import static cc.CodeCheckProp.PREVIOUS_BUTTON_TEXT;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import cc.data.Slide;
import cc.data.CodeCheckData;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_EDIT_SLIDER;
import static cc.style.CodeCheckStyle.CLASS_EDIT_TEXT_FIELD;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import static cc.style.CodeCheckStyle.CLASS_UPDATE_BUTTON;
import djf.ui.AppMessageDialogSingleton;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.FileHeader;


/**
 *
 *
 * @author Ammar Khandwala
 */
public class extractWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    HBox bottomToolbar;
    Button homeButton;
    Button nextButton;
    Button previousButton;
    Button removeButton;
    Button refreshButton;
    Button viewButton;
    Button extractButton;

    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> extractColumn;

    // THE EDIT PANE
    GridPane editPane;
    Label extractLabel;
    Label extractInfoLabel;
    Label extractProgressLabel;
    ProgressBar extract;
    TextFlow textArea;
    ScrollPane textAreaScrollPane;
    BorderPane workspaceBorderPane = new BorderPane();
    renameWorkspace rename;
    Text extractSuccess;
    private StringBuilder fieldContent, fieldContent1;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public extractWorkspace(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
        rename = new renameWorkspace(app);
        editImagesToolbar = new HBox();
        bottomToolbar = new HBox();
        homeButton = new Button(props.getProperty(HOME_BUTTON_TEXT));
        nextButton = new Button(props.getProperty(NEXT_BUTTON_TEXT));
        previousButton = new Button(props.getProperty(PREVIOUS_BUTTON_TEXT));
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        extractButton = new Button(props.getProperty(EXTRACT_BUTTON_TEXT));

        slidesTableScrollPane = new ScrollPane();
        textAreaScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        extractColumn = new TableColumn(props.getProperty(LABEL_EXTRACT_COLUMN_TEXT));
        editPane = new GridPane();
        extractLabel = new Label(props.getProperty(LABEL_EXTRACT));
        extractInfoLabel = new Label(props.getProperty(LABEL_EXTRACT_INFO));
        extractProgressLabel = new Label(props.getProperty(LABEL_EXTRACT_PROGRESS));
        extract = new ProgressBar();
        extractSuccess = new Text("Successfully Extracted Files\n");
        textArea = new TextFlow();
        textArea.setTextAlignment(TextAlignment.JUSTIFY);
        
        //textArea.getChildren().add(text2);

        // ARRANGE THE TABLE
        extractColumn = new TableColumn(props.getProperty(LABEL_EXTRACT_COLUMN_TEXT));

        slidesTableView.getColumns().add(extractColumn);

        extractColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(1));

        extractColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
        editImagesToolbar.getChildren().add(homeButton);
        editImagesToolbar.getChildren().add(previousButton);
        editImagesToolbar.getChildren().add(nextButton);
        bottomToolbar.getChildren().add(removeButton);
        bottomToolbar.getChildren().add(refreshButton);
        bottomToolbar.getChildren().add(viewButton);
        slidesTableScrollPane.setContent(slidesTableView);
        textAreaScrollPane.setContent(textArea);
        editPane.add(extractLabel, 0, 0);
        editPane.add(extractProgressLabel, 1, 0);
        editPane.add(extractInfoLabel, 0, 1);
        editPane.add(extractButton, 1, 1);
        editPane.add(slidesTableScrollPane, 0, 2);
        editPane.add(bottomToolbar, 0, 3);
        editPane.add(extract, 2, 0);
        editPane.add(textAreaScrollPane, 1, 2);

        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(editImagesToolbar);

        //disable Controls
        nextButton.setDisable(true);
        previousButton.setDisable(true);
        homeButton.setDisable(true);
        removeButton.setDisable(true);
        viewButton.setDisable(true);
        extractButton.setDisable(true);

        // workspaceBorderPane.setCenter(slidesTableScrollPane);
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        textAreaScrollPane.setFitToWidth(true);
        textAreaScrollPane.setFitToHeight(true);
        slidesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        slidesTableScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        textAreaScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        workspaceBorderPane.setCenter(editPane);

        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;
    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new CodeCheckController(app);

        nextButton.setOnAction((ActionEvent e) -> {
            controller.handleNext();
            previousButton.setDisable(false);
        });

        previousButton.setOnAction((ActionEvent e) -> {
            controller.handlePrevious();
        });

        homeButton.setOnAction((ActionEvent e) -> {
            controller.handleHomeRequest();
        });

        refreshButton.setOnAction((ActionEvent e) -> {
            controller.updateExtract();
        });
        
        slidesTableView.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() >= 1) {
                disableFields(false);

            }
        });

        extractButton.setOnAction((ActionEvent e) -> {
            
            extractFiles();
        });
        
        removeButton.setOnAction((ActionEvent e) -> {
            remove();
        });
        
        viewButton.setOnAction((ActionEvent e) -> {
            view();
        });

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {
        editImagesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        bottomToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        extractButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        homeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        nextButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        previousButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        extractInfoLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        extractProgressLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        extractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);

    }

    public void extractFiles() {
        fieldContent = new StringBuilder("");
        String destination = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions";
        File f = new File(destination);

        try {
            for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
                String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "blackboard/" + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName();
                ZipFile zipFile = new ZipFile(Path);
                zipFile.extractAll(destination);
                
            }
        } catch (ZipException e) {
            e.printStackTrace();
        }

        for (File listFile : f.listFiles()) {
            if (listFile.getName().endsWith(".zip")) {
                fieldContent.append(listFile.getName()).append("\n");
            }
        }
        Text text= new Text();
        Text extractError = new Text("Extraction Error");

        text.setText(fieldContent.toString());
        textArea.getChildren().clear();
        textArea.getChildren().addAll(extractSuccess, text);

    }
    
    

    @Override
    public void resetWorkspace() {

    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        homeButton.setDisable(false);
        nextButton.setDisable(false);

    }

    public void remove() {
        CodeCheckData data = (CodeCheckData) app.getDataComponent();

        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "blackboard/" + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
            f.delete();
        }
        data.getSlides().removeAll(slidesTableView.getSelectionModel().getSelectedItems());

    }
    
    public void view() {
        fieldContent1= new StringBuilder("");
        String entryName = new String("");
         AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
       // if (slidesTableView.getSelectionModel().selectionModeProperty().getValue().MULTIPLE)
       // System.out.println(slidesTableView.getSelectionModel().getSelectionMode().equals(SelectionMode.MULTIPLE));
        try {
            String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "blackboard" + "/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName();
            ZipInputStream zipinputstream = null;
            ZipEntry zipentry;
            zipinputstream = new ZipInputStream(
                    new FileInputStream(Path));

            zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) {
                //for each entry to be extracted
                entryName = zipentry.getName();
                fieldContent1.append(entryName).append("\n");
                zipinputstream.closeEntry();
                zipentry = zipinputstream.getNextEntry();
                
            }
            zipinputstream.close();
            ScrollPane scroll = new ScrollPane();
            Text text = new Text();
            text.setText(fieldContent1.toString());
            scroll.setContent(text);
            Scene scene = new Scene (scroll);
            dialog.setScene(scene);
            dialog.setTitle("Contents");
            dialog.show();
            //dialog.show("File Contents", fieldContent1.toString());
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void disableFields(Boolean value) {
        removeButton.setDisable(value);
        viewButton.setDisable(value);
        extractButton.setDisable(false);

    }
}
