/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.CODECHECK_BUTTON_TEXT;
import static cc.CodeCheckProp.LABEL_CODECHECK;
import static cc.CodeCheckProp.LABEL_CODECHECK_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_CODECHECK_INFO;
import static cc.CodeCheckProp.LABEL_CODECHECK_PROGRESS;
import static cc.CodeCheckProp.LABEL_UNZIP;
import static cc.CodeCheckProp.LABEL_UNZIP_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_UNZIP_INFO;
import static cc.CodeCheckProp.LABEL_UNZIP_PROGRESS;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.UNZIP_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import cc.data.CodeCheckData;
import cc.data.Slide;
import cc.data.codecheckSlide;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Ammar Khandwala
 */
public class codecheckWorkspace {
           CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    HBox bottomToolbar;
    Button homeButton;
    Button nextButton;
    Button previousButton;
    Button removeButton;
    Button refreshButton;
    Button viewButton;
    Button codeButton;
    Button viewResultsButton;
    private StringBuilder fieldContent1;

    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<codecheckSlide> slidesTableView;
    TableColumn<codecheckSlide, StringProperty> extractColumn;

    // THE EDIT PANE
    GridPane editPane;
    Label label;
    Label infoLabel;
    Label progressLabel;
    ProgressBar progress;
    TextFlow textArea;
    Text text2;
    ScrollPane textAreaScrollPane;
    BorderPane renameWorkspaceBorderPane ;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public codecheckWorkspace (CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
       
        bottomToolbar = new HBox();
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        codeButton = new Button(props.getProperty(CODECHECK_BUTTON_TEXT));
        viewResultsButton = new Button("View Results");
        slidesTableScrollPane = new ScrollPane();
        textAreaScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        extractColumn = new TableColumn(props.getProperty(LABEL_CODECHECK_COLUMN_TEXT));
        editPane = new GridPane();
        label = new Label(props.getProperty(LABEL_CODECHECK));
        infoLabel = new Label(props.getProperty(LABEL_CODECHECK_INFO));
        progressLabel = new Label(props.getProperty(LABEL_CODECHECK_PROGRESS));
        progress = new ProgressBar();
        text2 = new Text("Student Plagiarism Results can be found at:\n"); 
        textArea = new TextFlow();
        textArea.setTextAlignment(TextAlignment.JUSTIFY);
        textAreaScrollPane.setContent(textArea);
        // ARRANGE THE TABLE
        extractColumn = new TableColumn(props.getProperty(LABEL_CODECHECK_COLUMN_TEXT));

        slidesTableView.getColumns().add(extractColumn);

        extractColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(1));

        extractColumn.setCellValueFactory(
                new PropertyValueFactory<codecheckSlide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<codecheckSlide> model = data.getcodeCheckSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
      
        bottomToolbar.getChildren().add(removeButton);
        bottomToolbar.getChildren().add(refreshButton);
        bottomToolbar.getChildren().add(viewButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(label, 0, 0);
        editPane.add(progressLabel, 1, 0);
        editPane.add(infoLabel, 0, 1);
        editPane.add(codeButton, 1, 1);
        editPane.add(viewResultsButton, 2, 1);
        editPane.add(slidesTableScrollPane, 0, 2);
        editPane.add(bottomToolbar, 0, 3);
        editPane.add(progress, 2, 0);
        editPane.add(textAreaScrollPane, 1, 2);
        
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        
//        renameWorkspaceBorderPane = new BorderPane();
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        textAreaScrollPane.setFitToWidth(true);
        textAreaScrollPane.setFitToHeight(true);
        slidesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        slidesTableScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        textAreaScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
//        renameWorkspaceBorderPane.setCenter(editPane);
        
         
        

      
    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        codeButton.setOnAction((ActionEvent e) -> {
           performCodeCheck();
        });
        
        removeButton.setOnAction((ActionEvent e) -> {
           remove();
        });
        
         viewButton.setOnAction((ActionEvent e) -> {
           view();
        });
        
        viewResultsButton.setOnAction((ActionEvent e) -> {
           webview();
        });

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {
        
        bottomToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        codeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewResultsButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        label.getStyleClass().add(CLASS_PROMPT_LABEL);
        progressLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        infoLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
    }


    public GridPane getCodecheckWorkspace(){
        return editPane;
    }
    
   public void performCodeCheck(){
        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            String link = "<a href=\"https://media.giphy.com/media/iU2yJX5YKI9pe/giphy.gif</a>";
            Text text = new Text(link);
            textArea.getChildren().addAll(text2, text);
        }
   }
   
     public void remove() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();

        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/"+ "code/NetID/"+
                    slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
            if (f.exists()) {
                f.delete();
               data.getcodeCheckSlides().removeAll(slidesTableView.getSelectionModel().getSelectedItems());
            }
             else
            dialog.show("Remove Error", "File has not yet been extracted");

        }

    }
     
     private void webview() {
        Stage stage= new Stage();
        Text one = new Text("Web View");
        ScrollPane scroll = new ScrollPane();
        Text text = new Text();
        text.setText(one.toString());
        scroll.setContent(text);
        Scene scene = new Scene(scroll);
        

        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(browser);

        webEngine.load("https://media.giphy.com/media/iU2yJX5YKI9pe/giphy.gif");

        scene.setRoot(scrollPane);

        stage.setScene(scene);
        stage.show();
    }

    public void view() {
        fieldContent1 = new StringBuilder("");
        String entryName = new String("");
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        // if (slidesTableView.getSelectionModel().selectionModeProperty().getValue().MULTIPLE)
        // System.out.println(slidesTableView.getSelectionModel().getSelectionMode().equals(SelectionMode.MULTIPLE));

        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "code/NetID/"
                + "/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName();
        File file = new File(Path);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        fieldContent1.append(Arrays.toString(directories)).append("\n");

        ScrollPane scroll = new ScrollPane();
        Text text = new Text();
        text.setText(fieldContent1.toString());
        scroll.setContent(text);
        Scene scene = new Scene(scroll);
        dialog.setScene(scene);
        dialog.setTitle("Contents");
        dialog.show();


    }
    
    
}
