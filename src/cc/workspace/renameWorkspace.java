/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;

import static cc.CodeCheckProp.LABEL_EXTRACT_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_RENAME;
import static cc.CodeCheckProp.LABEL_RENAME_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_RENAME_INFO;
import static cc.CodeCheckProp.LABEL_RENAME_PROGRESS;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.RENAME_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import cc.data.CodeCheckData;
import cc.data.Slide;
import cc.data.renamedSlide;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import properties_manager.PropertiesManager;

/**
 *
 * @author Ammar Khandwala
 */
public class renameWorkspace  {
    
    
      CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    HBox bottomToolbar;
    Button homeButton;
    Button nextButton;
    Button previousButton;
    Button removeButton;
    Button refreshButton;
    Button viewButton;
    Button renameButton;

    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<renamedSlide> slidesTableView;
    TableColumn<renamedSlide, StringProperty> extractColumn;

    // THE EDIT PANE
    GridPane editPane;
    Label label;
    Label infoLabel;
    Label progressLabel;
    ProgressBar progress;
    TextFlow textArea;
    ScrollPane textAreaScrollPane;
    BorderPane renameWorkspaceBorderPane ;
    private StringBuilder fieldContent, fieldContent1;
    Text renameSuccess;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public renameWorkspace (CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }


    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
       
        bottomToolbar = new HBox();
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        renameButton = new Button(props.getProperty(RENAME_BUTTON_TEXT));

        slidesTableScrollPane = new ScrollPane();
        textAreaScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        extractColumn = new TableColumn(props.getProperty(LABEL_RENAME_COLUMN_TEXT));
        editPane = new GridPane();
        label = new Label(props.getProperty(LABEL_RENAME));
        infoLabel = new Label(props.getProperty(LABEL_RENAME_INFO));
        progressLabel = new Label(props.getProperty(LABEL_RENAME_PROGRESS));
        progress = new ProgressBar();
        textArea = new TextFlow();
        textArea.setTextAlignment(TextAlignment.JUSTIFY);
        textAreaScrollPane.setContent(textArea);
        renameSuccess = new Text("Successfully Renamed Files\n");
        // ARRANGE THE TABLE
        extractColumn = new TableColumn(props.getProperty(LABEL_RENAME_COLUMN_TEXT));

        slidesTableView.getColumns().add(extractColumn);

        extractColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(1));

        extractColumn.setCellValueFactory(
                new PropertyValueFactory<renamedSlide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<renamedSlide> model = data.getrenamedSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
      
        bottomToolbar.getChildren().add(removeButton);
        bottomToolbar.getChildren().add(refreshButton);
        bottomToolbar.getChildren().add(viewButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(label, 0, 0);
        editPane.add(progressLabel, 1, 0);
        editPane.add(infoLabel, 0, 1);
        editPane.add(renameButton, 1, 1);
        editPane.add(slidesTableScrollPane, 0, 2);
        editPane.add(bottomToolbar, 0, 3);
        editPane.add(progress, 2, 0);
        editPane.add(textAreaScrollPane, 1, 2);
        
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        textAreaScrollPane.setFitToWidth(true);
        textAreaScrollPane.setFitToHeight(true);
        slidesTableScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        textAreaScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        slidesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        removeButton.setDisable(true);
        viewButton.setDisable(true);
        renameButton.setDisable(true);
        
         
        

      
    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        
        
        renameButton.setOnAction((ActionEvent e) -> {
            renameSubmissions();
        });
        
         removeButton.setOnAction((ActionEvent e) -> {
             remove();
        });
         
         viewButton.setOnAction((ActionEvent e) -> {
             view();
        });
         
         slidesTableView.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() >= 1) {
                disableFields(false);

            }
        });
        

        

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {
        
        bottomToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        renameButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        label.getStyleClass().add(CLASS_PROMPT_LABEL);
        progressLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        infoLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
    }


    public GridPane getRenameWorkspace(){
        return editPane;
    }
    
    public void renameSubmissions() {
        String str = "";
        String netID = "";
        fieldContent = new StringBuilder("");
        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            String destination = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions/" + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName();
            File f = new File(destination);
            str = slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName();
            String[] split = str.split("_");
            if (split.length > 3) {
                netID = split[1] + ".zip";
                f.renameTo(new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions/" + netID));
            }
            
       
       // for (File listFile : f.listFiles()) {
            if (f.getName().endsWith(".zip")) {
                fieldContent.append(f.getName()).append("\n");
        //    }

        }

        }
        

        Text text = new Text();
        Text extractError = new Text("Extraction Error");

        text.setText(fieldContent.toString());
        textArea.getChildren().clear();
        textArea.getChildren().addAll(renameSuccess, text);

    }

    public void remove() {
        CodeCheckData data = (CodeCheckData) app.getDataComponent();

        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions/" + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
            f.delete();
        }
        data.getrenamedSlides().removeAll(slidesTableView.getSelectionModel().getSelectedItems());

    }

    public void view() {
        fieldContent1 = new StringBuilder("");
        String entryName = new String("");
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        try {
            String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions" + "/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName();
            ZipInputStream zipinputstream = null;
            ZipEntry zipentry;
            zipinputstream = new ZipInputStream(
                    new FileInputStream(Path));

            zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) {
                //for each entry to be extracted
                entryName = zipentry.getName();
                fieldContent1.append(entryName).append("\n");
                zipinputstream.closeEntry();
                zipentry = zipinputstream.getNextEntry();

            }
            zipinputstream.close();
            ScrollPane scroll = new ScrollPane();
            Text text = new Text();
            text.setText(fieldContent1.toString());
            scroll.setContent(text);
            Scene scene = new Scene(scroll);
            dialog.setScene(scene);
            dialog.setTitle("Contents");
            dialog.show();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void disableFields(Boolean value) {
        removeButton.setDisable(value);
        viewButton.setDisable(value);
        renameButton.setDisable(value);

    }

}
