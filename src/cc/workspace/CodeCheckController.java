package cc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import cc.CodeCheckApp;
import cc.data.CodeCheckData;
import java.lang.reflect.Array;
import javafx.event.ActionEvent;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Ammar Khandwala
 * @version 1.0
 */
public class CodeCheckController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CodeCheckApp app;
    Array[] classObjects;
    int currentIndex;
    renameWorkspace rename;
    unzipWorkspace unzip;
    sourceCodeWorkspace extractSource;
    codecheckWorkspace codecheck;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public CodeCheckController(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        classObjects = new Array[3];
        currentIndex = -1;
        rename = new renameWorkspace(app);
        unzip = new unzipWorkspace(app);
        extractSource = new sourceCodeWorkspace(app);
        codecheck = new codecheckWorkspace(app);

    }

    public void handleRemoveImage() {
        extractWorkspace workspace = (extractWorkspace) app.getWorkspaceComponent();
        TableView slidesTableView = workspace.slidesTableView;
    }

    public void updateExtract() {
        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "blackboard";
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        File f = new File(Path);
        File[] listOfFiles = f.listFiles();

        if (listOfFiles.length > 0) {
          
            for (int i = 0; i < listOfFiles.length; i++) {
                if (data.getSlides().isEmpty() || (!data.checkDuplicate(listOfFiles[i].getName()))) {
                    data.addSlide(listOfFiles[i].getName());
                }

            }
        } 
        else {
        }
    }
    
    public void updateRename(){
        
        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions";
        File f = new File(Path);
        File[] listOfFiles = f.listFiles();

        if (listOfFiles.length > 0) {
            CodeCheckData data = (CodeCheckData) app.getDataComponent();
            for (int i = 0; i < listOfFiles.length; i++) {
               { if ((data.getrenamedSlides().isEmpty() || (!data.checkRenameDuplicate(listOfFiles[i].getName())))&& (listOfFiles[i].getName().endsWith(".zip")))
                    data.addRenameSlide(listOfFiles[i].getName());
                    
                }
            }
        } else {
        }
    
    }
    
    public void updateUnzip(){
        
        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions";
        File f = new File(Path);
        File[] listOfFiles = f.listFiles();

        if (listOfFiles.length > 0) {
            CodeCheckData data = (CodeCheckData) app.getDataComponent();
            for (int i = 0; i < listOfFiles.length; i++) {
                if ((data.getUnzipSlides().isEmpty() || (!data.checkUnzipDuplicate(listOfFiles[i].getName())))
                        &&(listOfFiles[i].getName().endsWith(".zip"))){
                    data.addUnzipSlide(listOfFiles[i].getName());
                   
                }
             
            }
        } else {
        }
    
    }
    
    public void updateSourceCode() {

        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID";
        File f = new File(Path);
        File[] listOfFiles = f.listFiles();

        if (listOfFiles.length > 0) {
            CodeCheckData data = (CodeCheckData) app.getDataComponent();
            for (int i = 0; i < listOfFiles.length; i++) {
                 if ((data.getSourceSlides().isEmpty() || (!data.checkSourceDuplicate(listOfFiles[i].getName())))) {
                    data.addSourceSlide(listOfFiles[i].getName());

                }

            }
        } else {
        }

    }
    
    public void updateCodeCheck() {

        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "code/NetID";
        File f = new File(Path);
        File[] listOfFiles = f.listFiles();

        if (listOfFiles.length > 0) {
            CodeCheckData data = (CodeCheckData) app.getDataComponent();
            for (int i = 0; i < listOfFiles.length; i++) {
                 if ((data.getcodeCheckSlides().isEmpty() || (!data.checkCodecheckDuplicate(listOfFiles[i].getName())))) {
                    data.addCodeCheckSlide(listOfFiles[i].getName());

                }

            }
        } else {
        }

    }

    

    public void handleSelectSlide() {

    }

    void handleCaptionTextTyped() {

    }

    void handleSliderMoved() {

    }

    public void handleNext() {

        currentIndex++;
        if (currentIndex == 0) {
            app.getGUI().getAppPane().setCenter(rename.getRenameWorkspace());
            updateRename();

        }

        if (currentIndex == 1) {
            app.getGUI().getAppPane().setCenter(unzip.getUnzipWorkspace());
            updateUnzip();

        }

        if (currentIndex == 2) {
            app.getGUI().getAppPane().setCenter(extractSource.getExtractSourceWorkspace());
            updateSourceCode();
        }

        if (currentIndex == 3) {
            extractWorkspace extract = (extractWorkspace) app.getWorkspaceComponent();
            app.getGUI().getAppPane().setCenter(codecheck.getCodecheckWorkspace());
            updateCodeCheck();
            extract.nextButton.setDisable(true);
            extract.previousButton.setDisable(false);
        }

    }

    public void handlePrevious() {

        currentIndex--;
        extractWorkspace extract = (extractWorkspace) app.getWorkspaceComponent();
        extract.nextButton.setDisable(false);
        if (currentIndex == 2) {

            app.getGUI().getAppPane().setCenter(extractSource.getExtractSourceWorkspace());
           
        }

        if (currentIndex == 1) {
            app.getGUI().getAppPane().setCenter(unzip.getUnzipWorkspace());
           
            

        }

        if (currentIndex == 0) {
            app.getGUI().getAppPane().setCenter(rename.getRenameWorkspace());
            

        }

        if (currentIndex == -1) {

            app.getGUI().getAppPane().setCenter(extract.getWorkspace());
            extract.previousButton.setDisable(true);
            extract.nextButton.setDisable(false);
        }

    }

    public void handleHomeRequest() {
        extractWorkspace extract = (extractWorkspace) app.getWorkspaceComponent();
        app.getGUI().getAppPane().setCenter(extract.getWorkspace());
        currentIndex = -1;
        extract.previousButton.setDisable(true);
        extract.nextButton.setDisable(false);
    }
    
    public void handleRefresh() {
        rename.refreshButton.setOnAction((ActionEvent e) -> {
            updateRename();

        });

        unzip.refreshButton.setOnAction((ActionEvent e) -> {
            updateUnzip();

        });

        extractSource.refreshButton.setOnAction((ActionEvent e) -> {
            updateSourceCode();

        });

        codecheck.refreshButton.setOnAction((ActionEvent e) -> {
            updateCodeCheck();

        });

    }
}

