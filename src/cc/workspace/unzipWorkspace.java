/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.LABEL_RENAME;
import static cc.CodeCheckProp.LABEL_RENAME_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_RENAME_INFO;
import static cc.CodeCheckProp.LABEL_RENAME_PROGRESS;
import static cc.CodeCheckProp.LABEL_UNZIP;
import static cc.CodeCheckProp.LABEL_UNZIP_COLUMN_TEXT;
import static cc.CodeCheckProp.LABEL_UNZIP_INFO;
import static cc.CodeCheckProp.LABEL_UNZIP_PROGRESS;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.RENAME_BUTTON_TEXT;
import static cc.CodeCheckProp.UNZIP_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import cc.data.CodeCheckData;
import cc.data.unzipSlide;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import properties_manager.PropertiesManager;

/**
 *
 * @author Ammar Khandwala
 */
public class unzipWorkspace {
    CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    HBox bottomToolbar;
    Button homeButton;
    Button nextButton;
    Button previousButton;
    Button removeButton;
    Button refreshButton;
    Button viewButton;
    Button unzipButton;

    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<unzipSlide> slidesTableView;
    TableColumn<unzipSlide, StringProperty> extractColumn;

    // THE EDIT PANE
    GridPane editPane;
    Label label;
    Label infoLabel;
    Label progressLabel;
    ProgressBar progress;
    TextFlow textArea;
    ScrollPane textAreaScrollPane;
    BorderPane renameWorkspaceBorderPane ;
    private StringBuilder fieldContent, fieldContent1;
    Text extractSuccess;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public unzipWorkspace (CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
       
        bottomToolbar = new HBox();
        removeButton = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        unzipButton = new Button(props.getProperty(UNZIP_BUTTON_TEXT));

        slidesTableScrollPane = new ScrollPane();
        textAreaScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        extractColumn = new TableColumn(props.getProperty(LABEL_UNZIP_COLUMN_TEXT));
        editPane = new GridPane();
        label = new Label(props.getProperty(LABEL_UNZIP));
        infoLabel = new Label(props.getProperty(LABEL_UNZIP_INFO));
        progressLabel = new Label(props.getProperty(LABEL_UNZIP_PROGRESS));
        progress = new ProgressBar();
        textArea = new TextFlow();
        textArea.setTextAlignment(TextAlignment.JUSTIFY);
        textAreaScrollPane.setContent(textArea);
        extractSuccess = new Text("Successfully Unzipped Files\n");
        // ARRANGE THE TABLE
        extractColumn = new TableColumn(props.getProperty(LABEL_UNZIP_COLUMN_TEXT));

        slidesTableView.getColumns().add(extractColumn);

        extractColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(1));

        extractColumn.setCellValueFactory(
                new PropertyValueFactory<unzipSlide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<unzipSlide> model = data.getUnzipSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
      
        bottomToolbar.getChildren().add(removeButton);
        bottomToolbar.getChildren().add(refreshButton);
        bottomToolbar.getChildren().add(viewButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(label, 0, 0);
        editPane.add(progressLabel, 1, 0);
        editPane.add(infoLabel, 0, 1);
        editPane.add(unzipButton, 1, 1);
        editPane.add(slidesTableScrollPane, 0, 2);
        editPane.add(bottomToolbar, 0, 3);
        editPane.add(progress, 2, 0);
        editPane.add(textAreaScrollPane, 1, 2);
        
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        

        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        slidesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        textAreaScrollPane.setFitToWidth(true);
        textAreaScrollPane.setFitToHeight(true);
        slidesTableScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        textAreaScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        removeButton.setDisable(true);
        viewButton.setDisable(true);
        unzipButton.setDisable(true);
        
         
        

      
    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
       
         
        unzipButton.setOnAction((ActionEvent e) -> {
            unZip();
        });
        
         viewButton.setOnAction((ActionEvent e) -> {
            view();
        });
         
         removeButton.setOnAction((ActionEvent e) -> {
            remove();
        });
        
        
        
         slidesTableView.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() >= 1) {
                disableFields(false);

            }
        });

    

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {
        
        bottomToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        unzipButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        label.getStyleClass().add(CLASS_PROMPT_LABEL);
        progressLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        infoLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
    }


    public GridPane getUnzipWorkspace(){
        return editPane;
    }
    
    public void unZip() {
        fieldContent = new StringBuilder("");
        String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions/" +
                slidesTableView.getSelectionModel().getSelectedItem().getFileName();
        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
        File projects = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID/"
                + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
        projects.mkdir();
        try {
            ZipFile zipFile = new ZipFile(Path);

            List<FileHeader> fileHeaders = zipFile.getFileHeaders();

            for (FileHeader fileHeader : fileHeaders) {

                zipFile.extractFile(fileHeader, "work/" + app.getGUI().getWindow().getTitle() +
                        "/" + "projects/NetID/" 
                        + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName());
                
            }
        

        } catch (ZipException e) {
            e.printStackTrace();
        }
        }
        File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/" + "projects/NetID");
        for (File listFile : f.listFiles()) {
            
               fieldContent.append(listFile.getName()).append("\n");
                
            
        }
       
        Text text = new Text();
        Text extractError = new Text("Extraction Error");

        text.setText(fieldContent.toString());
        textArea.getChildren().clear();
        textArea.getChildren().addAll(extractSuccess, text);

    }
        
    public void view() {
        fieldContent1= new StringBuilder("");
        String entryName = new String("");
         AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
       // if (slidesTableView.getSelectionModel().selectionModeProperty().getValue().MULTIPLE)
       // System.out.println(slidesTableView.getSelectionModel().getSelectionMode().equals(SelectionMode.MULTIPLE));
        try {
            String Path = "work/" + app.getGUI().getWindow().getTitle() + "/" + "submissions" 
                    + "/" + slidesTableView.getSelectionModel().getSelectedItem().getFileName();
            ZipInputStream zipinputstream = null;
            ZipEntry zipentry;
            zipinputstream = new ZipInputStream(
                    new FileInputStream(Path));

            zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) {
                //for each entry to be extracted
                entryName = zipentry.getName();
                fieldContent1.append(entryName).append("\n");
                zipinputstream.closeEntry();
                zipentry = zipinputstream.getNextEntry();
                
            }
            zipinputstream.close();
            ScrollPane scroll = new ScrollPane();
            Text text = new Text();
            text.setText(fieldContent1.toString());
            scroll.setContent(text);
            Scene scene = new Scene (scroll);
            dialog.setScene(scene);
            dialog.setTitle("Contents");
            dialog.show();
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void remove() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();

        for (int i = 0; i < slidesTableView.getSelectionModel().getSelectedItems().size(); i++) {
            File f = new File("work/" + app.getGUI().getWindow().getTitle() + "/" 
                    + "projects/NetID/" + slidesTableView.getSelectionModel().getSelectedItems().get(i).getFileName()+".zip");
            if (f.exists()){
            f.delete();
            data.getUnzipSlides().removeAll(slidesTableView.getSelectionModel().getSelectedItems());
            }
            else
            dialog.show("Remove Error", "File has not yet been unzipped");
        }
       

    }
    
    private void disableFields(Boolean value) {
        removeButton.setDisable(value);
        viewButton.setDisable(value);
        unzipButton.setDisable(value);

    }
}


