package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import static sc.SlideshowCreatorProp.APP_PATH_WORK;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_MESSAGE;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_TITLE;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class SlideshowCreatorController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    SlideshowCreatorApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public SlideshowCreatorController(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    // CONTROLLER METHOD THAT HANDLES ADDING A DIRECTORY OF IMAGES
    public void handleAddAllImagesInDirectory() {
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            DirectoryChooser dirChooser = new DirectoryChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dirChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = dirChooser.showDialog(app.getGUI().getWindow());
            if (dir != null) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    String fileName = f.getName();
                    if (fileName.toLowerCase().endsWith(".png") ||
                            fileName.toLowerCase().endsWith(".jpg") ||
                            fileName.toLowerCase().endsWith(".gif")) {
                        String path = f.getPath();
                        String caption = "";
                        Image slideShowImage = loadImage(path);
                        int originalWidth = (int)slideShowImage.getWidth();
                        int originalHeight = (int)slideShowImage.getHeight();
                        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
                        data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                    }
                }
            }
        }
        catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }
    
    public void handleAddImage() {
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            FileChooser imageChooser = new FileChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            imageChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
            if (imageFile != null) {
                String fileName = imageFile.getName();
                if (fileName.toLowerCase().endsWith(".png") ||
                            fileName.toLowerCase().endsWith(".jpg") ||
                            fileName.toLowerCase().endsWith(".gif")) {
                    String path = imageFile.getPath();
                    String caption = "";
                    Image slideShowImage = loadImage(path);
                    int originalWidth = (int)slideShowImage.getWidth();
                    int originalHeight = (int)slideShowImage.getHeight();
                    SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
                    data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                }
            }
        }
        catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }
    
    public void handleRemoveImage() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        TableView slidesTableView = workspace.slidesTableView;
        Slide selectedSlide = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        data.removeSlide(selectedSlide);
        
        slidesTableView.getSelectionModel().clearSelection();
        workspace.enableSlideEditingControls(false);
    }
    
    public void handleUpdateSlide() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        workspace.updateButton.setDisable(true);
        
        TableView slidesTableView = workspace.slidesTableView;
        Slide selectedSlide = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        
        String caption = workspace.captionTextField.getText();
        int currentWidth = (int)workspace.currentWidthSlider.getValue();
        int currentHeight = (int)workspace.currentHeightSlider.getValue();
        
        selectedSlide.setCaption(caption);
        selectedSlide.setCurrentWidth(currentWidth);
        selectedSlide.setCurrentHeight(currentHeight);
        workspace.slidesTableView.refresh();
    }
    
    public void handleSelectSlide() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        Slide selectedSlide = workspace.slidesTableView.getSelectionModel().getSelectedItem();
        int selectedIndex = workspace.slidesTableView.getSelectionModel().getSelectedIndex();
        
        // WE ONLY RESPOND IF IT'S A SELECTION
        if (selectedIndex >= 0) {
            // LOAD ALL THE SLIDE DATA INTO THE CONTROLS
            workspace.fileNameTextField.setText(selectedSlide.getFileName());
            workspace.pathTextField.setText(selectedSlide.getPath());
            workspace.captionTextField.setText(selectedSlide.getCaption());
            workspace.originalWidthTextField.setText("" + selectedSlide.getOriginalWidth());
            workspace.originalHeightTextField.setText("" + selectedSlide.getOriginalHeight());
            workspace.currentWidthSlider.setValue(selectedSlide.getCurrentWidth().doubleValue());
            workspace.currentHeightSlider.setValue(selectedSlide.getCurrentHeight().doubleValue());
        
            workspace.enableSlideEditingControls(true);
        }
    }
    
    // THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
	File file = new File(imagePath);
	URL fileURL = file.toURI().toURL();
	Image image = new Image(fileURL.toExternalForm());
	return image;
    }

    void handleCaptionTextTyped() {
        SlideshowCreatorWorkspace gui = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        gui.updateButton.setDisable(false);
    }
    
    void handleSliderMoved() {
        SlideshowCreatorWorkspace gui = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        gui.updateButton.setDisable(false);        
    }
}